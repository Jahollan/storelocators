import promiseMap from 'p-map';
import cx from 'classnames';
import {getUserLocation, loadScript} from 'lib/utils';
import {Component} from 'preact';
import DirectionIcon from './DirectionIcon';
import SearchIcon from './SearchIcon';
import classNames from './StoreLocator.css';
import WebIcon from './WebIcon';

const units = {
  METRIC: 0,
  IMPERIAL: 1
};

const toMiles = 1.609;

export class StoreLocator extends Component {
  static defaultProps = {
    stores: [],
    zoom: 6,
    limit: 10,
    center: {lat: 52.1471243, lng: 4.882348},
    placheholderText: 'Please fill in a location...',
    homeLocationHint: 'Current location',
    premStoreMarkerIcon: './Premium-logo.png',
    storeMarkerIcon: './Normal-logo.svg',
    unitSystem: 'METRIC',
    farAwayMarkerOpacity: 0.6,
    fullWidthMap: false
  };

  constructor(props) {
    super(props);
    this.state = {
      searchLocation: null,
      activeStoreId: null,
      stores: this.addStoreIds(props.stores)
    };
    this.markers = [];
  }

  addStoreIds(stores = []) {
    return stores.map((store, i) => {
      store.id = store.id || i;
      return store;
    });
  }

  async loadGoogleMaps() {
    if (window.google && window.google.maps) return Promise.resolve();
    return loadScript(
      `https://maps.googleapis.com/maps/api/js?key=${this.props.apiKey}&libraries=geometry,places`
    );
  }

  loadStores = async searchLocation => {
    if (!this.props.loadStores) return this.state.stores;
    let stores = await this.props.loadStores(searchLocation);
    stores = this.addStoreIds(stores);
    this.setState({stores});
    return stores;
  };

  getMarkerIcon(icon) {
    if (!icon) return null;
    const {markerIconSize} = this.props;
    if (typeof icon === 'string' && markerIconSize) {
      const iconSize = markerIconSize;
      return {
        url: icon,
        scaledSize: new google.maps.Size(iconSize[0], iconSize[1])
      };
    }
    return icon;
  }

  addStoreMarker = store => {
    const infoWindow = new google.maps.InfoWindow({
      content: `<div class="${classNames.infoWindow}">
          <h4>${store.name}</h4>
          ${store.address}
        </div>`
    });
    let marker;
    if (store.typeStore == 'Premium') {
      marker = new google.maps.Marker({
        position: store.location,
        title: store.name,
        id: store.id,
        map: this.map,
        icon: this.getMarkerIcon(this.props.premStoreMarkerIcon)
      });
    } else {
      marker = new google.maps.Marker({
        position: store.location,
        title: store.name,
        id: store.id,
        map: this.map,
        icon: this.getMarkerIcon(this.props.storeMarkerIcon)
      });
    }

    marker.addListener('click', () => {
      if (this.infoWindow) {
        this.infoWindow.close();
      }
      infoWindow.open(this.map, marker);
      this.infoWindow = infoWindow;
      this.setState({activeStoreId: store.id});
    });
    this.markers.push(marker);
    return marker;
  };

  async getDistance(p1, p2) {
    const origin = new google.maps.LatLng(p1);
    const destination = new google.maps.LatLng(p2);
    const directDistance = this.getDirectDistance(origin, destination);
    return new Promise(resolve => {
      return resolve(directDistance);
    });
  }

  getDirectDistance(origin, destination) {
    const distance =
      google.maps.geometry.spherical.computeDistanceBetween(origin, destination) / 1000;
    if (units[this.props.unitSystem] === 1) {
      return {
        distance: distance / toMiles,
        distanceText: `${(distance / toMiles).toFixed(2)} mi`
      };
    }
    return {
      distance,
      distanceText: `${distance.toFixed(2)} km`
    };
  }

  setHomeMarker(location) {
    if (this.homeMarker) {
      this.homeMarker.setMap(null);
    }
    const infoWindow = new google.maps.InfoWindow({
      content: this.props.homeLocationHint
    });
    this.homeMarker = new google.maps.Marker({
      position: location,
      title: this.props.homeLocationHint,
      map: this.map,
      icon: this.getMarkerIcon(this.props.homeMarkerIcon)
    });
    this.homeMarker.addListener('click', () => {
      if (this.infoWindow) {
        this.infoWindow.close();
      }
      infoWindow.open(this.map, this.homeMarker);
      this.infoWindow = infoWindow;
    });
  }

  setupMap = async () => {
    const {center, zoom} = this.props;
    this.map = new window.google.maps.Map(this.mapFrame, {
      center,
      zoom,
      mapTypeControl: false,
      streetViewControl: false,
      fullscreenControl: false
    });
    // this.distanceService = new google.maps.DistanceMatrixService();
    const geocoder = new google.maps.Geocoder();
    this.setupAutocomplete();
    this.state.stores.map(this.addStoreMarker);
    const location = await getUserLocation();
    this.setState({searchLocation: location});
    // this.calculateDistance(location);
    this.map.setCenter(location);
    this.map.setZoom(11);
    this.setHomeMarker(location);

    geocoder.geocode({location: location}, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
          this.input.value = results[0].formatted_address;
        }
      }
    });
  };

  setupAutocomplete() {
    const autocomplete = new google.maps.places.Autocomplete(this.input);
    autocomplete.bindTo('bounds', this.map);
    autocomplete.setFields(['address_components', 'geometry', 'icon', 'name']);
    autocomplete.addListener('place_changed', () => {
      const place = autocomplete.getPlace();
      if (!place.geometry) return;

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        this.map.fitBounds(place.geometry.viewport);
      } else {
        this.map.setCenter(place.geometry.location);
        this.map.setZoom(11);
      }
      const location = place.geometry.location.toJSON();
      this.setState({searchLocation: location});
      this.setHomeMarker(location);
      this.calculateDistance(location);
    });
  }

  clearMarkers() {
    this.markers.forEach(m => {
      m.setMap(null);
    });
    this.markers = [];
  }

  async calculateDistance(searchLocation) {
    const {limit} = this.props;
    if (!searchLocation) return this.props.stores;
    const stores = await this.loadStores(searchLocation);
    const data = await promiseMap(stores, store => {
      return this.getDistance(searchLocation, store.location).then(result => {
        Object.assign(store, result);
        return store;
      });
    });
    let result = data.sort((a, b) => a.distance - b.distance);
    const bounds = new google.maps.LatLngBounds();
    bounds.extend(searchLocation);
    this.clearMarkers();
    result = result.map((store, i) => {
      store.hidden = i + 1 > limit;
      const marker = this.addStoreMarker(store);
      if (store.hidden) {
        marker.setOpacity(this.props.farAwayMarkerOpacity);
      } else {
        bounds.extend(store.location);
        let foundStore = null;
        this.markers.forEach(element => {
          if (element.title === store.name) {
            foundStore = element;
          }
        });
        if (foundStore !== null) {
          google.maps.event.trigger(foundStore, 'click');
        }
      }
      return store;
    });
    this.map.fitBounds(bounds);
    this.map.setCenter(bounds.getCenter());
    this.setState({stores: result});
  }

  componentDidMount() {
    this.loadGoogleMaps()
      .then(this.loadStores)
      .then(this.setupMap);
  }

  onStoreClick({location, id, name}) {
    this.map.setCenter(location);
    this.map.setZoom(11);
    this.setState({activeStoreId: id});
    this.clearOpacity();
    let foundStore = null;
    this.markers.forEach(element => {
      if (element.title === name) {
        foundStore = element;
      }
    });
    if (foundStore !== null) {
      google.maps.event.trigger(foundStore, 'click');
    }
  }

  clearOpacity() {
    this.markers.forEach(marker => {
      marker.setOpacity(1);
    });
  }
  componentDidUpdate() {
    const element = document.getElementById(this.state.activeStoreId);
    console.log(element);
    if (element !== null) {
      element.scrollIntoView({behavior: 'smooth', block: 'nearest', inline: 'start'});
    }
  }

  //noinspection JSCheckFunctionSignatures
  render({searchHint, placheholderText, travelMode, fullWidthMap}, {activeStoreId, stores}) {
    return (
      <div className={cx(classNames.container, {[classNames.fullWidthMap]: fullWidthMap})}>
        <div className={classNames.searchBox}>
          <div className={classNames.searchInput}>
            <input type="text" placeholder={placheholderText} ref={input => (this.input = input)} />
          </div>
          {searchHint && <div className={classNames.searchHint}>{searchHint}</div>}
          <ul className={classNames.storesList}>
            {stores.map(store => {
              const locationStr = `${store.location.lat},${store.location.lng}`;
              return (
                <li
                  id={store.id}
                  key={store.id}
                  onClick={() => this.onStoreClick(store)}
                  className={cx({
                    [classNames.activeStore]: store.id === activeStoreId,
                    [classNames.hiddenStore]: store.hidden
                  })}
                >
                  <h4>{store.name}</h4>
                  {store.distanceText && (
                    <div className={classNames.storeDistance}>
                      {store.distanceText} away{' '}
                      {store.durationText &&
                        `(${store.durationText} by ${travelModes[travelMode]})`}
                    </div>
                  )}
                  <address>
                    {store.address}, {store.city}, {store.country}
                  </address>
                  <div className={classNames.storeActions} onClick={e => e.stopPropagation()}>
                    <a
                      target="_blank"
                      href={`https://www.google.com/maps/search/?api=1&query=${store.name}+${store.city}+${store.zipCode}+${store.country}`}
                    >
                      <DirectionIcon />
                      directions
                    </a>{' '}
                    {store.website && (
                      <a target="_blank" href={store.website}>
                        <WebIcon />
                        website
                      </a>
                    )}
                  </div>
                </li>
              );
            })}
          </ul>
        </div>
        <div className={classNames.map} ref={mapFrame => (this.mapFrame = mapFrame)} />
      </div>
    );
  }
}
